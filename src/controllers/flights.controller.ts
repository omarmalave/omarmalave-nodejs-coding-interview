import {
    JsonController,
    Get,
    Param,
    Put,
    Post,
    Body,
} from 'routing-controllers';
import { FlightsService } from '../services/flights.service';

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    private _flightsService: FlightsService;

    constructor() {
        this._flightsService = new FlightsService();
    }

    @Get('/passengers')
    async getAllPassengers() {
        return {
            status: 200,
            data: await this._flightsService.getPassengers(),
        };
    }

    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await this._flightsService.getFlights(),
        };
    }

    @Put('/:code')
    async updateFlightStatus(@Param('code') code: string, @Body({ validate: true })
    flight: {
        status: string;
    },) {
        return {
            status: 200,
            data: await this._flightsService.updateFlightStatus(code, flight.status),
        };
    }


    @Put('/:code/passengers')
    async updateFlightPassengers(@Param('code') code: string, @Body({ validate: true })
    flight: {
        passengers: string[];
    },) {
        return {
            status: 200,
            data: await this._flightsService.updateFlightPassengers(code, flight.passengers),
        };
    }

    // add flight
    @Post('')
    async addFlight(
        @Body({ validate: true })
        flight: {
            code: string;
            origin: string;
            destination: string;
            status: string;
        },
    ) {
        return {
            status: 200,
            data: await this._flightsService.addFlight(flight),
        };
    }
}
