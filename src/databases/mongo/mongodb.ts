import { Database } from '../database_abstract';
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

import { FlightsModel } from './models/flights.model';
import { loadData } from '../../../scripts/base-data';
import { PassengersModel } from './models/passengers.model';

export class MongoStrategy extends Database {
    constructor() {
        super();
        this.getInstance();
    }

    private async getInstance() {
        const mongo = await MongoMemoryServer.create();
        const uri = mongo.getUri();

        const mongooseOpts = {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        };

        // const flights = [
        //     {
        //         code: 'ABC-123',
        //         origin: 'EZE',
        //         destination: 'LDN',
        //         status: 'ACTIVE',
        //     },
        //     {
        //         code: 'XYZ-678',
        //         origin: 'CRC',
        //         destination: 'MIA',
        //         status: 'ACTIVE',
        //     },
        // ];

        (async () => {
            await mongoose.connect(uri, mongooseOpts);
            await loadData()
        })();
    }

    public async getFlights() {
        return FlightsModel.find({}).populate('passengers');
    }

    public async getPassengers() {
        return PassengersModel.find({});
    }

    public async addFlight(flight: {
        code: string;
        origin: string;
        destination: string;
        status: string;
    }) {
        const newFlight = await FlightsModel.create({ ...flight })
        return newFlight._id;
    }

    public async updateFlightStatus(code: string, status: string) {
        const updateFlight = await FlightsModel.findOneAndUpdate({ code }, { $set: { status } }, { new: true })
        return updateFlight?._id
    }

    public async updateFlightPassengers(code: string, passengers: string[]) {
        // subclass must implement this method
        const updateFlight = await FlightsModel.findOneAndUpdate({ code }, { $addToSet: { passengers } }, { new: true })
        return updateFlight
    }
}
