import mongoose, { Schema } from 'mongoose';

interface Passenger {
    name: string;
    gender: string;
    email: string;
}

const schema = new Schema<Passenger>(
    {
        name: { required: true, type: String },
        gender: { required: true, type: String },
        email: { required: true, type: String },
    },
    { timestamps: true },
);

export const PassengersModel = mongoose.model('Passengers', schema);
