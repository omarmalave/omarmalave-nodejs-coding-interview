import { Database } from '../databases/database_abstract';
import { DatabaseInstanceStrategy } from '../database';

export class FlightsService {
    private readonly _db: Database;

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance();
    }

    public async getFlights() {
        return this._db.getFlights();
    }

    public async getPassengers() {
        return this._db.getPassengers();
    }

    public async updateFlightStatus(code: string, status: string) {
        return this._db.updateFlightStatus(code, status);
    }


    public async updateFlightPassengers(code: string, passengers: string[]) {
        return this._db.updateFlightPassengers(code, passengers);
    }

    public async addFlight(flight: {
        code: string;
        origin: string;
        destination: string;
        status: string;
    }) {
        return this._db.addFlight(flight);
    }
}
