import { PassengersModel } from '../src/databases/mongo/models/passengers.model'
import { FlightsModel } from '../src/databases/mongo/models/flights.model'

import personsJson from './persons.json'
import flightsJson from './flights.json'

export const loadData = async () => {
    for (const person of personsJson) {
        await PassengersModel.create(person)
    }

    console.log(` --- Loaded ${personsJson.length} persons ---`)

    for (const flight of flightsJson) {
        await FlightsModel.create(flight)
    }

    console.log(` --- Loaded ${flightsJson.length} flights ---`)
}
